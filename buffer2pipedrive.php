<?php

// Переменные, которые ты можешь настроить на свой вкус

include __DIR__ . '/config.php';

// Создаём папки для хранения записей

if (!file_exists($bufferFolder)) {
  consoleLog('Collect some data with zadarma2buffer.php first');
  die;
}
if (!file_exists($resultsFolder) || !file_exists($resultsFolder)) {
  mkdir($resultsFolder, 0777);
}

// Проходим по очереди по всем файлам в буфере и каждый отправляем в Pipedrive

$files = scandir($bufferFolder);
foreach ($files as $filename) {
  if (!in_array($filename, ['.', '..'])) {
    // Отправляем в Pipedrive
    if (!sendToPipedrive($filename)) {
      //consoleLog("Отправка записи в Pipedrive не удалась");
      //die;
    } else {
      consoleLog("Запись $filename отправлена в Pipedrive успешно");
    }
  }
}

// Отправляем файл в Pipedrive, при удачной отправке перемещаем в result
function sendToPipedrive($filename) {
  global $bufferFolder, $resultsFolder;

  // Достаём запись из файла
  $item = json_decode(file_get_contents("$bufferFolder/$filename"), true);

  if (!$item) {
    consoleLog("Не получилось загрузить запись $filename из файла");
    return false;
  }

  $name = $item['disposition'];
  if (strlen($item['sip']) == 3) {
    if ($item['disposition'] == 'busy') {
      $name = 'Занято';
    } else if ($item['disposition'] == 'no answer') {
      $name = 'Не взял трубку';
    } else if ($item['disposition'] == 'answered') {
      $name = 'Разговор ' . seconds2hhmmss($item['seconds']);
    } else {
      $name = 'Недозвон';
    }
  } else {
    if ($item['disposition'] == 'answered') {
      $name = 'Входящий разговор ' . seconds2hhmmss($item['seconds']);
    } else {
      $name = 'Пропущенный входящий';
    }
  }

  // Формируем лид для api Pipedrive  
  // у исходящих сохраняем caller id
  if (strlen($item['sip']) == 3) {
    $name .= ' ' . $item['clid'] . ' (' . $item['sip'] . ')';
  } else {
    $name .= ' ' . $item['sip'];
  }

  $name .= ' -> ' . $item['destination'];

  if ($item['is_recorded'] == 'true') {
    $name .= ' http://pbx.icoach.io/' . $item['call_id'] . '.mp3';
  }

  $leads['request']['leads']['add'] = [
    [
      'name' => $name
    ]
  ];

  consoleLog('Сохраняем в Pipedrive звонок ' . $name);

  // Отправляем в Pipedrive
//  $out = amoApiRequest('v2/json/leads/set', 'POST', $leads);
//  $res = json_decode($out, true);

  // Если удачно отправили, то перемещаем файл в папку result
  if ($res && $res['response'] && $res['response']['leads']) {
    //return rename("$bufferFolder/$filename", "$resultsFolder/$filename");
  } else {
    consoleLog('Не удалось сохранить');
    //consoleLog($out);
  }

  return false;
}

function seconds2hhmmss($seconds) {
  $t = round($seconds);
  return sprintf('%02d:%02d:%02d', ($t/3600),($t/60%60), $t%60);
}

function consoleLog($text)
{
  echo date('[Y-m-d H:i:s] ');
  echo "$text\n";
}