<?php

// Переменные, которые ты можешь настроить на свой вкус

include __DIR__ . '/config.php';

require __DIR__ . '/zadarma-client.php';

// Создаём папки для хранения записей

if (!file_exists($bufferFolder)) {
  mkdir($bufferFolder, 0777);
}
if (!file_exists($recordsFolder)) {
  mkdir($recordsFolder, 0777);
}

// Подключаемся к Zadarma

$zd = new \Zadarma_API\Client($zadarmaKey, $zadarmaSecret);

// Получаем статистику звонков

$start = date('Y-m-d H:i:s', strtotime('-2 days'));

$out = $zd->call('/v1/statistics/pbx/', ['start' => $start], 'get');

// Декодируем статистику

$records = json_decode($out, true);

// Проверяем успешность получения статистики

if ($records['status'] !== 'success' || !array_key_exists('stats', $records)) {
  consoleLog($out);
  consoleLog("Не удалось получить статистику у Zadarma");
  die;
}

// Сохраняем каждую запись из Zadarma в файл в папке bufferFolder

consoleLog("Получено всего " . count($records['stats']) . " записей из Zadarma");

foreach ($records['stats'] as $item) {
  saveItem($item);
}

/*
// Проходим по очереди по всем файлам в буфере и каждый отправляем в Pipedrive

$files = scandir($bufferFolder);
foreach ($files as $filename) {
  if (!in_array($filename, ['.', '..'])) {
    // Отправляем в АМО
    if (!sendToAmo($filename)) {
      consoleLog("Отправка записи в АМО не удалась");
      die;
    } else {
      consoleLog("Запись $filename отправлена в Pipedrive успешно");
    }
  }
}
*/

// Сохраняет полученную от Zadarma запись в файл в папке bufferFolder
function saveItem($item) {
  global $bufferFolder, $resultFolder;

  $call_id = $item['call_id'];
  consoleLog("Запись $call_id");

  // Если уже есть файл для такой записи, повторно не сохраняем
  if (file_exists("$bufferFolder/$call_id") || file_exists("$resultFolder/$call_id")) {
    return;
  }

  // Кодируем в JSON

  $data = json_encode($item, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE);

  consoleLog($data);

  // если звонок записан — получаем ссылку на запись

  if ($item['is_recorded'] == 'true') {
    saveRecord($call_id);
  }

  // Сохраняем в файл

  file_put_contents("$bufferFolder/$call_id", $data);

  consoleLog("Получена новая запись $call_id из Zadarma");
}

function saveRecord($call_id) {
  global $zd, $recordsFolder;

  consoleLog("Скачиваем запись звонка $call_id");

  $out = $zd->call('/v1/pbx/record/request/', ['call_id' => $call_id], 'get');
  $records = json_decode($out, true);

  if ($records['status'] !== 'success' || !array_key_exists('link', $records)) {
    consoleLog($out);
    consoleLog("Не удалось получить запись звонка у Zadarma");
    die;
  }

  $res = downloadFile($records['link'], "$recordsFolder/$call_id.mp3");

  if ($res === true) {
    consoleLog("Файл с записью скачан");
    return true;
  } else {
    consoleLog("Не удалось скачать файл");
    consoleLog($res);
    return false;
  }
}

function downloadFile($url, $dest)
{
  $options = array(
    CURLOPT_FILE => is_resource($dest) ? $dest : fopen($dest, 'w'),
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_URL => $url,
    CURLOPT_FAILONERROR => true, // HTTP code > 400 will throw curl error
  );

  $ch = curl_init();
  curl_setopt_array($ch, $options);
  $return = curl_exec($ch);

  if ($return === false) {
    return curl_error($ch);
  } else {
    return true;
  }
}

/*
// Отправляем файл в АМО, при удачной отправке перемещаем в result
function sendToAmo($filename) {
  global $bufferFolder, $resultFolder;

  // Достаём запись из файла
  $item = json_decode(file_get_contents("$bufferFolder/$filename"), true);

  if (!$item) {
    consoleLog("Не получилось загрузить запись $filename из файла");
    return false;
  }

  // Формируем лид для api Pipedrive
  $name = $item['from'] . ' -> ' . $item['to'];
  $leads['request']['leads']['add'] = [
    [
      'name' => $name
    ]
  ];
  consoleLog('Сохраняем в Pipedrive звонок ' . $name . ' от ' . $item['callstart']);

  // Отправляем в АМО
  $out = amoApiRequest('v2/json/leads/set', 'POST', $leads);
  $res = json_decode($out, true);

  // Если удачно отправили, то перемещаем файл в папку result
  if ($res && $res['response'] && $res['response']['leads']) {
    return rename("$bufferFolder/$filename", "$resultFolder/$filename");
  } else {
    consoleLog($out);
  }

  return false;
}

*/

function consoleLog($text)
{
  echo date('[Y-m-d H:i:s] ');
  echo "$text\n";
}