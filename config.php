<?php

// Данные для доступа в Zadarma
$zadarmaKey = '***';
$zadarmaSecret = '***';

// Данные для доступа в Pipedrive
$pipedriveToken = '***';

// Папка для временного хранения вытащенных из Zadarma, но ещё не засунутых в Pipedrive записей
$bufferFolder = __DIR__ . '/buffer';

// Папка для постоянного хранения аудиозаписей звонков
$recordsFolder = __DIR__ . '/records';

// Папка для постоянного хранения уже засунутых в Pipedrive записей
$resultsFolder = __DIR__ . '/results';

// Временная зона для отчёта
date_default_timezone_set('Europe/Moscow');
