<?php

require __DIR__ . '/vendor/autoload.php';

use \Devio\Pipedrive\Pipedrive; // https://github.com/IsraelOrtuno/pipedrive

echo phone2person($argv[1])."\n";

include __DIR__ . '/config.php';

function phone2person($phone) {
  // Токен надо загрузить из конфига
  $pipedrive = new Pipedrive($pipedriveToken);

  if (!$name) $name = $phone;

  echo "name: $name, phone: $phone\n";

  // Получаем список клиентов из Pipedrive

  $res = $pipedrive->persons->all();
  $pipepersons = $res->getData();

  echo count($pipepersons) . " persons loaded\n";

/// DEBUG ME!!!

  $getPerson = function($name, $phone) use ($pipepersons, $pipedrive) {
    function isSameValue($value1, $value2) {
      if (!$value1 || !$value2) return false;
      return $value1 == $value2;
    }

    foreach ($pipepersons as $p) {
      $pp = get_object_vars($p);
      if (isSameValue($phone, $p->phone[0]->value)
      || isSameValue($phone, russianIntlPhoneToLocal($p->email[0]->value))) {
        return $p->id;
      }
    }

    return "no person found for phone $phone";

    $o = $pipedrive->persons->add([
      'name' => $name,
      'phone' => $phone
    ]);

    return $o->getData()->id;
  };

/*
  // Создаём сделку

  $title = $fieldValue(['niche']);
  if (empty($title)) $title = 'Deal';

  $deal = $pipedrive->deals->add([
    'title' => $title,
    'value' => $list['value'],
    'user_id' => 2002004,//$list['user'],
    'person_id' => $getPerson($name, $phone, $email, $vk, $skype),
    'stage_id' => 10//$list['stage']
  ])->getData();

  $note = $pipedrive->notes->add([
    'content' => $note,
    'deal_id' => $deal->id,
  ])->getData();
*/
}

function removeEmoji($text) {
  $clean_text = "";

  // Match Emoticons
  $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
  $clean_text = preg_replace($regexEmoticons, '', $text);

  // Match Miscellaneous Symbols and Pictographs
  $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
  $clean_text = preg_replace($regexSymbols, '', $clean_text);

  // Match Transport And Map Symbols
  $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
  $clean_text = preg_replace($regexTransport, '', $clean_text);

  // Match Miscellaneous Symbols
  $regexMisc = '/[\x{2600}-\x{26FF}]/u';
  $clean_text = preg_replace($regexMisc, '', $clean_text);

  // Match Dingbats
  $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
  $clean_text = preg_replace($regexDingbats, '', $clean_text);

  return $clean_text;
}

function russianIntlPhoneToLocal($phone) {
  if (strlen($phone) == 11 && substr($phone, 0, 1) == '7') {
    return '8' . substr($phone, 1);
  } else {
    return $phone;
  }
}
